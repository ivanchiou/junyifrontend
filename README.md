<div align="center">
<h1>Junyi Academy Frontend</h1>

<a href="https://junyiacademy.org">
  <img
    width="300px"
    alt="junyi academy logo"
    src="https://www.junyiacademy.org/images/logo_256.png"
  />
</a>

<p>Junyi Academy is a nonprofit organization. This project is our open source project for Frontend.</p>

<br />
</div>

<hr />

<!-- prettier-ignore-start -->
<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![MIT License][license-badge]][license]
[![All Contributors](https://img.shields.io/badge/all_contributors-1-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->
<!-- prettier-ignore-end -->

## Contributors ✨

Thanks goes to these wonderful people ([emoji key][emoji-key]):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
    <td align="center"><a href="https://gitlab.com/leo.lin1"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/6003090/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>林暐唐</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/junyifrontend/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/junyifrontend/commits/master" title="Documentation">📖</a> <a href="#infra-leo.lin1" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a> <a href="https://gitlab.com/junyiacademy/junyifrontend/commits/master" title="Tests">⚠️</a></td>
  </tr>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors][all-contributors] specification.
Contributions of any kind welcome!

## LICENSE

[MIT][license]

<!-- prettier-ignore-start -->
[license-badge]: https://img.shields.io/badge/license-MIT-green?style=flat-square
[license]: https://gitlab.com/junyiacademy/junyifrontend/-/blob/master/LICENSE
[emoji-key]: https://allcontributors.org/docs/en/emoji-key
[all-contributors]: https://github.com/all-contributors/all-contributors

<!-- prettier-ignore-end -->
