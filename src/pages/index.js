/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { StudentLayout } from '@/packages/layout'
import { requireDeveloper } from '@/packages/permission'
import { fetchPermission, getAuthToken } from '@/utils'

function Index() {
  return <StudentLayout>{/* page content start */}</StudentLayout>
}

export async function getServerSideProps({ req }) {
  const cookies = req.headers.cookie || ''

  const serverSideAuthToken = getAuthToken(cookies)

  const [permission] = await Promise.all([fetchPermission(cookies)])

  return {
    props: {
      serverSideAuthToken,
      initialReduxState: {
        permission,
      },
    }, // will be passed to the page component as props
  }
}

export default requireDeveloper(Index)
