/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { BaseLayout } from '@/packages/layout'
import { LoginContainer } from '@/packages/permission/containers'

function Login() {
  return (
    <BaseLayout>
      <LoginContainer />
    </BaseLayout>
  )
}

export default Login
