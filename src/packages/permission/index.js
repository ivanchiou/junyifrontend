/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import authorization from './hoc/authorization'

export const requireAdmin = authorization('admin')
export const requireDeveloper = authorization('developer')
export const requireModerator = authorization('moderator')
export const requireUser = authorization('user')
