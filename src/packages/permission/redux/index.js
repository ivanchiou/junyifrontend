/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export {
  default as reducer,
  loginAsync,
  loginSuccess,
  loginFailure,
  fetchPermissionAsync,
  fetchPermissionSuccess,
  fetchPermissionFailure,
} from './slice'
export { default as epic } from './epics'
