/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

import Footer from './Footer'

export default {
  title: 'Layout/Footer',
  component: Footer,
}

const Template = (args) => <Footer {...args} />

export const Primary = Template.bind({})
Primary.args = {}
