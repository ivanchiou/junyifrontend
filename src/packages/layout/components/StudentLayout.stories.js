/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

import StudentLayout from './StudentLayout'

export default {
  title: 'Layout/StudentLayout',
  component: StudentLayout,
}

const Template = (args) => <StudentLayout {...args} />

export const Primary = Template.bind({})
Primary.args = {}
