/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { render, screen } from '@testing-library/react'
import StudentLayout from '../StudentLayout'

describe('StudentLayout', () => {
  it('renders without crashing', () => {
    render(<StudentLayout />)
    expect(screen.getByRole('img', { name: 'logo' })).toBeInTheDocument()
  })
})
