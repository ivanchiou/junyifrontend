/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// utils

// assets

// actions
import { fetchCourseMenuAsync } from '../redux'

// components
import CourseMenuComponent from '../components/CourseMenu'

// self-defined-components

const CourseMenu = ({
  subjects,
  fetchCourseMenuAsync,
  courseMenuAnchorEl,
  onCourseMenuClose,
}) => {
  React.useEffect(() => {
    if (subjects.length === 0) {
      fetchCourseMenuAsync()
    }
  }, [subjects, fetchCourseMenuAsync])

  return (
    <CourseMenuComponent
      subjects={subjects}
      courseMenuAnchorEl={courseMenuAnchorEl}
      onCourseMenuClose={onCourseMenuClose}
    />
  )
}

CourseMenu.propTypes = {
  subjects: PropTypes.array.isRequired,
  fetchCourseMenuAsync: PropTypes.func.isRequired,
  courseMenuAnchorEl: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.any }),
  ]),
  onCourseMenuClose: PropTypes.func,
}

const mapStateToProps = (state) => ({
  subjects: state.courseMenu.subjects,
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ fetchCourseMenuAsync }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(CourseMenu)
