/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { combineEpics, ofType } from 'redux-observable'
import { of, from } from 'rxjs'
import { map, mergeMap, catchError } from 'rxjs/operators'

import {
  fetchCourseMenuAsync,
  fetchCourseMenuSuccess,
  fetchCourseMenuFailure,
} from './slice'

export const courseMenu = (action$, state$, { get }) =>
  action$.pipe(
    ofType(fetchCourseMenuAsync.type),
    mergeMap(() =>
      from(get('/api/course-menu', {})).pipe(
        map((response) => fetchCourseMenuSuccess(response.data)),
        catchError((error) => of(fetchCourseMenuFailure(error)))
      )
    )
  )

export default combineEpics(courseMenu)
