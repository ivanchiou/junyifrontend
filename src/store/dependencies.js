/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import axios from 'axios'

export const get = (endpoint, query) => axios.get(endpoint, { params: query })
export const post = (endpoint, query) => axios.post(endpoint, query)
