/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { combineEpics } from 'redux-observable'
import { catchError } from 'rxjs/operators'

// epics
import { epic as permissionEpic } from '@/packages/permission/redux'
import { epic as courseMenuEpic } from '@/packages/courseMenu/redux'

const epics = [permissionEpic, courseMenuEpic]

const rootEpic = (action$, store$, dependencies) =>
  combineEpics(...epics)(action$, store$, dependencies).pipe(
    catchError((error, source) => {
      console.error(error)
      return source
    })
  )

export default rootEpic
